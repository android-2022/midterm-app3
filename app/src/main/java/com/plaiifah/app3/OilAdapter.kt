/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.plaiifah.app3

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.plaiifah.app3.model.Oil


fun list(): List<Oil>{
    return listOf<Oil>(
        Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20",36.84),
        Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91",37.68),
        Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95",37.95),
        Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95",45.44),
        Oil("เชลล์ ดีเซล B20",36.34),
        Oil("เชลล์ ฟิวเซฟ ดีเซล",36.34),
        Oil("เชลล์ ฟิวเซฟ ดีเซล B7",36.34),
        Oil("เชลล์ วี-เพาเวอร์ ดีเซล",36.34),
        Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7",47.06),
    )
}
class OilAdapter(private val context: Context, private val mList: List<Oil>) : RecyclerView.Adapter<OilAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_view, parent, false)

        return ViewHolder(view)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = list()[position]
        holder.textView1.text = ItemsViewModel.price.toString()
        holder.textView2.text = ItemsViewModel.name
        holder.item.setOnClickListener {
            val nwText = SpannableStringBuilder("${list()[position].name} \n price: ${list()[position].price.toString()} บาท/ลิตร")
            nwText.setSpan(RelativeSizeSpan(1.35f), 0, nwText.length, 0)
            Toast.makeText(this.context,nwText,Toast.LENGTH_LONG).show()

        }
    }
    override fun getItemCount(): Int {
        return mList.size
    }
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView1: TextView = itemView.findViewById(R.id.price)
        val textView2: TextView = itemView.findViewById(R.id.name)
        val item: LinearLayout = itemView.findViewById(R.id.card)
    }
}


